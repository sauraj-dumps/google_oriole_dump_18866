## oriole-user 12 SD1A.210817.015.A4 7697517 release-keys
- Manufacturer: google
- Platform: gs101
- Codename: oriole
- Brand: google
- Flavor: oriole-user
- Release Version: 12
- Id: SD1A.210817.015.A4
- Incremental: 7697517
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/oriole/oriole:12/SD1A.210817.015.A4/7697517:user/release-keys
- OTA version: 
- Branch: oriole-user-12-SD1A.210817.015.A4-7697517-release-keys
- Repo: google_oriole_dump_18866


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
